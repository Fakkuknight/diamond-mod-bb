
# How To Install ?

Simple :
* Download latest release zip file : https://gitlab.com/GreatNameAgain/diamond-mod-bb/tags
* Uncompress in BB root directory. You can choose the content to unzip. I added my named save files as well if you want to enjoy.
* You need the dll

Advanced :
* Install Git
* Clone the directory into your bb folder for easy updates, for this open a command line then browse to the BB directory.
* `git clone https://gitlab.com/GreatNameAgain/diamond-mod-bb.git .`
* to update simply do `git pull`

### Special save files instruction

Named save files are given to easy access to the game previous opportunities. Before extracting you'll have to backup your save somewhere and clean your save directory

# New Shortcuts

* `t` to talk with people instead of clicking the icon
* `tab` fast forward. Will stop at a choice
* `tab + ctrl` fast forward. Will automatically continue with choice 1.

# Content Spoils :

              
| Title | Description | Where to find it | Conditions | Credits |
| :-----: | ----------- | ---------------- | ------------|------|
| Mom cook naked | a new opportunity that goes through time | Ask mom to cook naked all the time | Unlocks after you've seen mom naked 3 times in the kitchen | [skyphernaut][GreatName]
| [ANIMATION INSIDE !] Ask mom to show off her jeans | Uses the new animation system | Tell mom you now what you want! | after 2nd movie with mom | [penecultor]
| Give Mom a hand for her bra | | Mom's changing and you stay to watch | Mom needs to have allowed you to stay when she's changing | [skyphernaut]
| Mom, hand massage is not enough | | When mom gives you a hand job in HER room after massage tell her you want more | | 
| Let's go shopping v0.2 [By Derover & Rich] | You can now go shopping with mom and the girls|Saturday 11 am when girls leave or open mom room at 10 am ||[By Derover & Rich]
| Mom I can't sleep Again | Ask mom to watch TV at night with you | Mom room 1 or 2 am |  Mom must be lustful enough, unlocks after alice catch you with mom and kira watching the movie.  |[skyphernaut]
| Kira and Alice afternight | Kira and Alice are having fun in bath after the night club | Bathroom saturday 8am || [FunFiction]
| Punish Alice in bathroom | You can now punish alice at night in bath. Don't push too much .. otherwise | Midnight alice in bathroom|Ask mom to punish alice this day and wait for midnight, don't do it on fridays|[celtic1138][GreatName]
|Alice after club enhanced |Extended option when Alice comes back drunk from the club|You can now enter Alice from the 3 options in shower| Alice drunk from nightclub|[celtic1138]
| Are you tired Mom ?| A new opportunity when massaging mom | Mom in her room, ask for handling your hard on in your room [TODO] : depending on Alice and Lisa's position you'll have some variations | Mom must be lustful enough, unlocks after alice catch you with mom and kira watching the movie | [Iceridlah][Rich]
| Have a peek in mom's room at night | Let you turn on the light when mom and kira are sleeping | Moms room with kira at 3am, Does not work when kira sleeps back in the living room | Mom blown you once | [FunFiction][GreatName]
| Max wants to go jogging | Max goes for a jogging, meanwhile kira is somehow angry against mom | Tell mom you know what you want ! |  Mom must be lustful enough, unlocks after alice catch you with mom and kira watching the movie | [FunFiction][GreatName]
|  Have a peek in mom's room in the morning  | Let you sneak in to kira when she's sleeping | Moms room with kira alone at 6am. Does not work when kira sleeps back in the living room in 0.9 | Kira naked | [?]
| Why so gloomy Alice ? | Let Alice enjoy or be punished when she's gloomy | When alice is at the terrace | You should ask to private punish alice in the morning if you want the punish option | [celtic1138][GreatName]
| Peep and open the door | Lisa is the best before school | 10am during the week at bedroom | after kira 3rd lesson | [skyphernaut][GreatName]
| New pair of Lisa images | Because she can be even more sexy | mostly 6am and 8am in bedroom | Sleeping naked and wearing no panties at home  |[skyphernaut]



# Changelog

2018.09.13.A
* Fixed with 0.13
* Added new shortcuts

2017.10.23.A :
* Added mom cook opportunity ! 
* --- Modding ---
* Added the possibility to create new plans from dialogues
* Added possibility to add direct dialogues to plans without having to go to a subdialogue
* Added new variables for mom cook story

2017.10.16.A :
* Added new mom wear cloth opportunity
* --- Modding ---
* Added ANIMATIONS ! Still rough and not optimized but working.
* Added the possibility to check for character position
* Added the possibility to use decrypted files directly by specifying either jpg or png.



2017.10.02.A : 
* Changed version numbers to be more naturally ordered
* Added Mom changing opportunity when naked
* Added Mom, hand massage is not enough
* Added let's go shopping v0.2

20.09.2017.A :
* Added Let's go shopping v0.1 [By Derover & Rich]

19.09.2017.B :
* Added can't sleep again
* Added Kira and Alice having fun

19.09.2017.A :
* Added Alice punishment in bathroom
* Added extended sex when alice comes back drunk from the club
* Removed others from Mods.txt to better handle merging
* Lots of fixes to other dialogues

11.09.2017 :
* First entry of the change log
* Added "Max Wants to go jogging" opportunity
* Lisa 10am opportunity now have more Fan Arts.
* Other opportunities that are implemented:
    * Mom and Kira at night 3am
    * Kira in the morning 6am
    * Lisa has some updated pictures
    * You can punish Alice at the pool if you ask mom in the morning

# Modding Tutorial

These are the additional options given by Diamond mod, have a look at Mod.txt for the full scenes examples.

##### No-Encryption image needed anymore
* Use .jpg or .png directly in Images directory without encryption, simply add `.jpg` or `.png` to image name.

* eg: `mom.cloth.15@68@ann-max-cloth-offer-9.jpg@<i>Mom manages to push you back</i>...`
#####Create Animations
* To create an animation create a folder with the animation name in Images `my-animation` and put a serie of jpg files named from `1.jpg, 2.jpg, ..., N.jpg` where N is the number of file. Then replace dialogue image with `my-animation.anim.N`
* eg: `mom.cloth.17@64@forced-ann-cowgirl.anim.15@Annnnhnnn ... Oh my god ....`

##### Create new plans (Like a new event when you enter the kitchen)
* You can create new plans, you have to take care there is only one plan changing option per character in Mod.txt, to change plan create a dialogue as follow 
```
    change.[mom or kira or alice or lisa].plan.dia@60@Plan change
	conditions&link^hour planName place picture special
```
* eg, for mom cook plan
```
change.mom.plan.dia@60@These are the new plans
	diam.mom.cook.naked=1&mom.cook.naked.01^8 cook2 4 y-mc-1 true
```

##### Create new dialogue talks (dialogues that can appear at any time)
* You can create new dialogues, you have to take care there is only one plan changing option per character in Mod.txt, to add new talks create a dialogue as follow 
```
diam.talks.init.[mom or kira or lisa or alice]@60@These are additional init talks
	conditions&link^

link.init@60@RU|This is the link dialogue will point to. init word is mandatory and answer line as well or it won't work.
	leave!^init hack

after link it will go to dialogue 
link@60@This is the next dialogue after init, it's a normal dialogue without .init in the link
```

* eg:  Ask mom to cook always naked
```
 diam.talks.init.mom@60@These are mom additional init talks
	diam.mom.cook.naked=0&fartuk>1&ask.mom.cook.naked^

ask.mom.cook.naked.init@60@RU|♦ About the fact you cook breakfast naked...
	leave!^init hack

ask.mom.cook.naked@68@I'm sorry son you had to see me this way!
	ask.mom.cook.naked.02^Don't be sorry! I like it!
	leave!^Nevermind, It's allright mom. I'll go.

```




# Assembly-CSharp mods :

These are the modification needed in order to use the modding possibilities, you need to use a software like DnSpy to apply them to the dll each time there is a new update of the game.

```c#
//G.cs
//This allow to check characters positions 
//eg. mom.pos=3

public static int checkPos(string name)
{
	for (int i = 0; i < G.npcs; i++)
	{
		if (G.npc[i].name == name)
		{
			return G.npc[i].pos;
		}
	}
	UnityEngine.Debug.Log("ERROR: checkAtHome: NPC `" + name + "` not found!");
	return 0;
}

//G.cs:get:29

if (varname.Contains(".pos"))
{
    return G.checkPos(varname.Split(new char[]
    {
        '.'
    })[0]);
}

```

```c#

//game.cs:imageLoader:~66 before load = string.Empty
//This adds animation !!

this.initAnimation();

//update:20 after imageLoader
if (this.animation != string.Empty)
{
    this.playAnimation();
}

// Token: 0x060000FF RID: 255
	private void initAnimation()
	{
		string text = global::GUI.load;
		if (text.Contains(".anim"))
		{
			string[] split = text.Split(new char[]
			{
				'.'
			});
			this.animation = split[0];
			this.currentAnim = 1;
			this.maxAnim = int.Parse(split[2]);
			this.playAnimation();
			return;
		}
		if (!text.Contains(this.animation + "\\"))
		{
			this.animation = string.Empty;
		}
	}

	// Token: 0x06000100 RID: 256
	private void playAnimation()
	{
		global::GUI.load = string.Concat(new object[]
		{
			this.animation,
			"\\",
			this.currentAnim,
			".jpg"
		});
		this.currentAnim = this.currentAnim % this.maxAnim + 1;
	}

    	// Token: 0x04000120 RID: 288
	private string animation = string.Empty;

	// Token: 0x04000121 RID: 289
	private int currentAnim = 1;

	// Token: 0x04000122 RID: 290
	private int maxAnim;

```

```
//New variables 
//V.cs 

		"diam.mom.fucks_-100_-100_1000", //will be used in the futur
		"diam.mom.cook.fuck",
		"diam.mom.cook.naked",
```

```
//Add create variable in operatParams, just after split - WARNING : Not working very well better to do manual addition like previously

		if (array[0] == "create" && G.get(array[1]) == -1000000)
		{
			int val = (array.Length > 2) ? Convert.ToInt32(array[2]) : 0;
			int min = (array.Length > 3) ? Convert.ToInt32(array[3]) : 0;
			int max = (array.Length > 4) ? Convert.ToInt32(array[4]) : 1000;
			G.newSP(array[1], val, min, max);
		}


```



```
//New actions
//G.cs:action:216 replace, used for debuging
 string str2 = (dia3 == null) ? ("error" + array5[0] + ".init") : dia3.text[global::GUI.lang];

//G.cs:GetTalkList: (method:22) after array3 declaration and before any test. Replace num = 0

	int num = G.getTalkListAdditional(npcid, array3);

 // Token: 0x060001C5 RID: 453
	public static int getTalkListAdditional(string npc, string[] talks)
	{
		Dia dia = G.getDia("diam.talks.init." + npc);
		if (dia == null)
		{
			return 0;
		}
		int num = 0;
		for (int i = 0; i < dia.answers; i++)
		{
			Answer ans = dia.answer[i];
			if (ans.conditionOk())
			{
				talks[num++] = ans.link;
			}
		}
		return num;
	}

	// Token: 0x060001C6 RID: 454
	public static int getTalkListAdditional(int npcid, string[] talks)
	{
		if (npcid == L.mom)
		{
			return G.getTalkListAdditional("mom", talks);
		}
		if (npcid == L.liza)
		{
			return G.getTalkListAdditional("lisa", talks);
		}
		if (npcid == L.alisa)
		{
			return G.getTalkListAdditional("alice", talks);
		}
		if (npcid == L.kira)
		{
			return G.getTalkListAdditional("kira", talks);
		}
		return 0;
	}

```

```

//Add change plan at the end of OperateNewDay
	G.changePlanNpc(G.mom, G.getDia("change.mom.plan.dia"));
	G.changePlanNpc(G.liza, G.getDia("change.lisa.plan.dia"));
	G.changePlanNpc(G.alisa, G.getDia("change.alice.plan.dia"));
	G.changePlanNpc(G.kira, G.getDia("change.kira.plan.dia"));
	G.changePlanNpc(G.olivia, G.getDia("change.olivia.plan.dia"));

private static void changePlanNpc(Npc npc, Dia dia)
{
	if (dia == null)
	{
		return;
	}
	for (int i = 0; i < dia.answers; i++)
	{
		Answer answer = dia.answer[i];
		if (answer.conditionOk())
		{
			string[] planData = answer.text[0].Split(new char[]
			{
				' '
			});
			npc.changePlan(G.day, Convert.ToInt32(planData[0]), new Plan(planData[1], Convert.ToInt32(planData[2]), planData[3], true, answer.link), 1);
		}
	}
}

	
	
```


```
 //In G.checkKeyboard
 //Added "t" for talking, tab for passing and Tab+LeftShift for forced passing


//after escape
			if (Input.GetKeyUp("t"))
			{
				G.button("act-talk");
				return;
			}
	
	//after "event" replace space
	if ((global::GUI.keys == 1 && (Input.GetKeyUp("space") || Input.GetKey(KeyCode.Tab))) || (Input.GetKey(KeyCode.Tab) && Input.GetKey(KeyCode.LeftShift)))
			{
				G.button(global::GUI.key[0, 1]);
			}
	
```